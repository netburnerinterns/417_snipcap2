/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <predef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <startnet.h>
#include <autoupdate.h>
#include <ftpd.h>
#include <htmlfiles.h>
#include <fdprintf.h>
#include "DeviceSpec.h"

/*****************************************************************************
         FILE READ/LIST FUNCTIONS
******************************************************************************/

/*-----------------------------------------------------------------------
 * Get Directory String
 * This function is used to format a file name into a directory string
 * that the FTP client will recognize. In this very simple case we
 * are just going to hardcode the values.
 *-----------------------------------------------------------------------*/
void getdirstring( char *FileName, long FileSize, char *DirStr ) {
    char tmp[80];

    DirStr[0] = '-';  // '-' for file, 'd' for directory
    DirStr[1] = 0;

    // permissions, hard link, user, group
    strcat( DirStr, "rw-rw-rw- 1 user group " );

    siprintf( tmp, "%9ld ", FileSize );
    strcat( DirStr, tmp );

    strcat( DirStr, "JAN 01 2000 " );

    strcat( DirStr, FileName );
}

/*-----------------------------------------------------------------------
 *  This function is called by the FTP Server in response to a FTP Client's
 *  request to list the files (e.g. the "ls" command)
 *------------------------------------------------------------------------*/
int FTPD_ListFile(const char *current_directory,
        void *pSession, FTPDCallBackReportFunct *pFunc, int handle) {
    char DirStr[256];

    // Only one file exists ReadFile.txt
    getdirstring( DEFAULT_FILENAME, CAP_BUFF_SIZE, DirStr );
    pFunc( handle, DirStr );
    return FTPD_OK;
}

/*-----------------------------------------------------------------------
 *  This function is called by the FTP Server in response to a FTP
 *  Client's request to receive a file. In this example, only ReadFile.txt
 *  is available for download, and it's contents are hard coded to the
 *  string in the writestring() function.
 *-----------------------------------------------------------------------*/
int FTPD_SendFileToClient(const char *full_directory,
        const char *file_name, void *pSession, int fd) {
    // Only one file exists
    if ( strcmp( file_name,DEFAULT_FILENAME ) == 0 ) {
        return FTPD_OK;
    } else {
        return FTPD_FAIL;
    }
}


/*--------------------------------------------------------------------------
 *  This function is called by the FTP Server to determine if a file exists.
 *-------------------------------------------------------------------------*/
int FTPD_FileExists( const char *full_directory,
        const char *file_name, void *pSession ) {
    // Only one file exists
    if ( strcmp( file_name, DEFAULT_FILENAME ) == 0 ) {
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/*****************************************************************************
         FILE SEND FUNCTIONS
******************************************************************************/
/* This example only allows one file to be sent to the board. The file
 * contents are sent to the com1 serial port to be displayed; the file is
 * not stored in memory.   The method you would use to store a file depends
 * on your application. One way is to allocate memory and store the file
 * data as an array of bytes. A second way is to add a full file system.
 */
char tmp_resultbuff[255];

/* This function reads the data stream from the input stream file descriptor fd
 * and displays it to stdout, which is usually the com1 serial port on the
 * NetBurner board.
 */
void ShowFileContents( int fdr ) {
    iprintf( "\r\n[" );
    int rv;
    do {
        rv = ReadWithTimeout( fdr, tmp_resultbuff, 255, 20 );
        if ( rv < 0 ) {
            iprintf( "RV = %d\r\n", rv );
        }
        else {
            tmp_resultbuff[rv] = 0;
            iprintf( "%s", tmp_resultbuff );
        }
    }
    while ( rv > 0 );
    iprintf( "]\r\n" );
}

/*--------------------------------------------------------------------
 *  This function gets called by the FTP Server when a FTP client
 *  sends a file. File must be named WriteFile.txt.
 *--------------------------------------------------------------------*/
int FTPD_GetFileFromClient(const char *full_directory,
        const char *file_name, void *pSession, int fd) {
    if ( strcmp( file_name, "WriteFile.txt" ) == 0 ) {
        ShowFileContents( fd );
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/*-------------------------------------------------------------------------
 *  This function gets called by the FTP Server to determine if it is ok to
 *  create a file on the system. In this case is will occur when a FTP
 *  client sends a file. File must be named WriteFile.txt.
 *-------------------------------------------------------------------------*/
int FTPD_AbleToCreateFile(const char *full_directory,
        const char *file_name, void *pSession) {
    if ( strcmp( file_name, "WriteFile.txt" ) == 0 ) {
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/*-------------------------------------------------------------------
 * The parameters passed to you in this function show the entered
 * user name, password and IP address they came from. You can
 * modify this function any way you wish for authentication.
 *
 * Return Values:
 *   0   = Authentication failed
 *   > 0 = Authentication passed
 * -----------------------------------------------------------------*/
void * FTPDSessionStart( const char *user,
        const char *passwd, const IPADDR hi_ip ) {
    return ( void * ) 1; //  Return a non zero value
}

/* FTPD Functions that are not supported/used in this trivial case */
void FTPDSessionEnd( void *pSession ) {
    /* Do nothing */
}

int FTPD_ListSubDirectories(const char *current_directory,
        void *pSession, FTPDCallBackReportFunct *pFunc, int handle) {
    /* No directories to list */
    return FTPD_OK;
}

int FTPD_DirectoryExists( const char *full_directory, void *pSession ) {
    if(*full_directory=='\0') {
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

int FTPD_CreateSubDirectory(const char *current_directory,
        const char *new_dir, void *pSession) {
    return FTPD_FAIL;
}

int FTPD_DeleteSubDirectory(const char *current_directory,
        const char *sub_dir, void *pSession) {
    return FTPD_FAIL;
}

int FTPD_DeleteFile( const char *current_directory,
        const char *file_name, void *pSession ) {
    return FTPD_FAIL;
}

int FTPD_Rename( const char *current_directory,
        const char *cur_file_name,const char *new_file_name,void *pSession ) {
    return FTPD_FAIL;
}

int FTPD_Size( const char *full_directory, const char *file_name, int socket) {
    fdprintf( socket, "213 %ld\r\n", 1 );
    return FTPD_OK;
}
