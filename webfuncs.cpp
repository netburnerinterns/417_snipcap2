/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */
#include <stdlib.h>
#include <startnet.h>
#include "Stream.h"
#include "ConnToWeb.h"

extern "C" {
    void ProcessUserRequest( int sock, PCSTR url );
    void getFName(int sock);
    void getThisFile (int sock);
    void WebListDir(int sock, const char *dir, char* pName);
    void waitTilWrite();
}

/*
 * Function: waitTilWrite
 * ----------------------
 * this function delays the confirm/download page displayed after finishing
 * capture until everything is written to the SD card.
 */
void waitTilWrite () {
    if (streaming == 2) {
        OSSemPend(&DL_SEM, 0);
    }
}

/*
 * Function: getFName
 * ------------------
 * get user define or default file name to display on the website
 */
void getFName(int sock) {
    char* fname = getFileName();
    char buffer[300];
    siprintf(buffer, "<p>Current file name : %s.pcap </p>", fname);
    writestring(sock, buffer);
}

/*
 * Function: ProcessUserRequest
 * ----------------------------
 * get user request on UI and do stuff with it
 */
void ProcessUserRequest( int sock, PCSTR url ) {
    // get request
    int request;

    if ( *(url + 9) == '?' ) {
        request = atoi ( (char *) (url + 10) );
    } else {
        request = 0;
    }

    // call function
    switch (request) {
        case 1: if (getRefreshLock() == 0) {
                    StartStreaming();
                    lockRefresh();
                }
                break;
        case 2: if (getRefreshLock() == 1) {
                    StopStreaming();
                    unlockRefresh();
                }
                break;
        case 3: ResetStreaming();
                break;
    }
}

