/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/******************************************************************************
 417_SniPCAP_vr2.7

 This application captures Ethernet packages in the traffic in promiscuous
 mode, stream them to SD card until the user download it through the ftp client

 Hardware specification changes can be made in DeviceSpec.h
 Default : 50MB memory buffer (module memory)
           2 KB SRAM buffer (module RAM)
           4 GB file size limit for each file (FAT32)

 Version Log
 1.0 - 6/26/2015
   + added filter: if dest of the package is set to MY_MAC_ADDR
                  do nothing with it
   + added web ui: start,stop,rest,download button, and functionality)
   + changed name SnaPCAP -> SniPCAP
   + download fixed - no longer take user straight to ftp

 1.1 - 6/30/2015
   + structure fixes
   - main organized, cleaned
 1.2 - 7/6/2015
   + changed libraries for hardware switch enable;
   + EnablePromiscuous() added
   + public license added

 2.0 - 7/17/2015
   + integration of SD card feature - now streaming!
   - 4GB storage per file
 2.1 - 7/17/2015
   + streamed captured file is now downloadable through ftp server
   - web UI associated directly to the buffer deleted
 2.2 - 7/21/2015
   + file system display, download from it
   + fixed nBurn example where effs does not properly work with directories
     (improper url decoding ex. %20 -> ' ', and not recurse through DIR)
   + effs_time added
   + DeviceSpec and ftps.cpp separated from main
   + now displaying size
   + now its able to start a new file by starting the capture
   + capture file name is now editable
 2.3 - 7/28/2015
   + fixed so that landing page is index.htm
 2.4 - 8/4/2015
   + fixed bug on refreshing page with "?1" URL argument
   + placed lock on refresh page
   + implemented ajax and websocket to dynamically load page
   + myDoGet pext initialization replaced
 2.5 - 8/5/2015
   + fixed crash on initial load with "?1" URL argument
   + revisited UI design, added in-progress gif
   + fixed /DIR // by running /pcbin/longfilename.bat
 2.6 - 8/7/2015
   + major refactoring, major code clean up
 2.7 - 8/10/2015
   + got rid of AJAX call; found a way to use variable straight from the system
   + improved debug message on failure DeviceSpec comment
   + stream() renamed to streamFunc()
   + fixed logic flaw in stream task, while goes inside of if
   + added download button on finish capture
   + added link to readme + source on main page

 // TODO :
  * worry about actual date of the file and capture
  * you can add additional filter to reduce traffic and increase stability
  * for larger traffic
  *     See TODO down below

 *****************************************************************************/
#include "predef.h"
#include <autoupdate.h>
#include <startnet.h>
#include <sim.h>
#include <ethervars.h>
#include <ftpd.h>
#include <init.h>
#include <smarttrap.h>
#include "FileSystemUtils.h"
#include "Stream.h"
#include "ConnToWeb.h"

extern "C" {
    void UserMain(void * pd);
    void StreamTask(void *p);
    void RegisterPost();
}

const char * AppName = "417_SniPCAP_vr2.7";

void UserMain(void * pd) {
    init();
    EnablePromiscuous();
    EnableSmartTraps();

    OSSemInit(&BUF_SEM, 0);
    OSSemInit(&WRITE_SEM, 0);
    OSSemInit(&SOC_SEM, 0);
    OSSemInit(&DL_SEM, 0);

    iprintf("Application: %s\r\nNNDK Revision: %s\r\n", AppName ,GetReleaseTag());

    f_enterFS();
    drv = OpenOnBoardFlash();
    f_chdrive(drv);
    OSSimpleTaskCreate(StreamTask, WRITE_PRIO);
    OSChangePrio( HTTP_PRIO );
    f_enterFS();
    OSChangePrio( FTPD_PRIO );
    f_enterFS();
    OSChangePrio( MAIN_PRIO );

    StartHTTP();
    iprintf( "Starting FTP Server on port 21 ..." );
    FTPDStart( 21, FTPD_PRIO );
    iprintf( " complete\r\n" );

    TheWSHandler = MyDoWSUpgrade;
    OSSimpleTaskCreate(SocketTask, SOCKET_PRIO );

    RegisterPost();

    while (1) {
            OSTimeDly(TICKS_PER_SECOND);
    }
}

