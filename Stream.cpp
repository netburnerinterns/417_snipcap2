/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include "predef.h"
#include <netinterface.h>
#include <netrx.h>
#include <iosys.h>
#include "FileSystemUtils.h"
#include "Stream.h"

extern "C" {
    void StreamTask(void *p);
}

struct packet {
    DWORD lenPdata;
    DWORD time;
    DWORD timeFrac;
    BYTE pData[];
} __attribute__( ( packed ) );

packet* packetToStore;

int streaming = 0;
uint32_t CapturedNum = 0;
uint32_t finalInSD = 0;

OS_SEM WRITE_SEM;
OS_SEM BUF_SEM;
OS_SEM DL_SEM;

static BYTE capBuffer[CAP_BUFF_SIZE];
static BYTE sdBuffer[SD_BUFF_SIZE] FAST_USER_VAR;

static uint32_t bufferIndex = 0;
static uint32_t totalP = 0;
static uint32_t totalInSD = 0;
static uint32_t capBuffIndex = 0;

typedef int (*netDoRXFunc)(PoolPtr, WORD, int);

struct fileHeader {
    DWORD MAGIC;     // 0xa1b2c3d4
    WORD MAJORVR;    // 0x0002
    WORD MINORVR;    // 0x0004
    DWORD THISZONE;  // 0x00000000
    DWORD SIGFIGS;   // 0x00000000
    DWORD SNAPLEN;   // 0x00400000
    DWORD NETWORK;   // 0x00000001
} __attribute__( ( packed ) );

// Populate file header
fileHeader header = {0xa1b2c3d4, 0x0002, 0x0004,
    0x00000000, 0x00000000 , 0x00400000, 0x00000001};

struct pcapPHeader {
    DWORD ts_sec;
    DWORD ts_usec;
    DWORD incl_len;
    DWORD orig_len;
    BYTE pData[];
} __attribute__( ( packed ) );


/*****************************************************************************
         PACKET CAPTURE FUNCTIONS
******************************************************************************/
/*
 * Function: clearedNetDoRX
 * ------------------------
 * Another CustomNetDoRX, does nothing with the incoming packets
 */
int clearedNetDoRX(PoolPtr pp, WORD EthFrameSize, int if_num) {
    return 0;
}


/*
 * Function: PauseCapture
 * ----------------------
 * Pause capturing and store packages to capBuffer in case of buffer overflow.
 */
int PauseCapture() {
    iprintf("\r\nBuffer Shortage! Wrapping Up\n");
    iprintf ("Please wait until all packets are stored111\r\n");
    streaming = 2;  // done streaming, confirmation phase
    SetCustomNetDoRX(clearedNetDoRX);
    return 0;
}

/*
 * Function: myNetDoRX
 * -------------------
 * stores each incoming packet to capBuffer in the memory storing
 * length, time, timeFrac, and the data respectively
 */
int myNetDoRX(PoolPtr pp, WORD EthFrameSize, int if_num) {

    InterfaceBlock *pifb = GetInterFaceBlock();

    uint32_t START = (uint32_t) capBuffer;
    uint32_t END = (uint32_t) capBuffer + CAP_BUFF_SIZE;
    
    // TODO:: you can add additional filter here as needed
    // filter if (dest == my MAC)
    if((memcmp(&(((EFRAME*)pp->pData) -> dest_addr),
           &pifb->theMac, sizeof(&pifb->theMac))) == 0) {
        return 0;
    }

    // location current packet placement in the memory
    packetToStore = (packet*) (capBuffer + bufferIndex);

    // populating packet data in the memory
    packetToStore->lenPdata = EthFrameSize;
    packetToStore->time = pp->dwTime;
    packetToStore->timeFrac = pp->dwTimeFraction;
    memcpy(packetToStore->pData, pp->pData, EthFrameSize);

    // check fail conditions of circular buffer 
    if ((capBuffIndex > bufferIndex)
            && (capBuffIndex - bufferIndex) < BUFF_PADDING) {
        PauseCapture();
        unlockRefresh();
    }
    if ((bufferIndex > capBuffIndex)
            && ((END - bufferIndex) + (capBuffIndex - START)) < BUFF_PADDING) {
        PauseCapture();
        unlockRefresh();
    }

    // prevent overflow; be circular
    if (bufferIndex + EthFrameSize
            + sizeof(packet) + BUFF_PADDING > CAP_BUFF_SIZE) {
        bufferIndex = 0;
    } else {
        bufferIndex += (EthFrameSize + sizeof(packet));
    }

    totalP += 1;

    OSSemPost(&BUF_SEM);

    return 0;
}


/*
 * Function: StartCapture
 * ----------------------
 * begins capturing and storing packages to capBuffer in the memory.
 */
void StartCapture() {
    iprintf("\r\n Start Capturing \r\n");
    SetCustomNetDoRX(myNetDoRX);
}



/*****************************************************************************
         STREAM TO SD CARD FUNCTIONS
******************************************************************************/
/*
 * Function: StartStreaming
 * --------------------
 * Connected to UI, indicate start of streaming
 */
void StartStreaming() {
    streaming = 1;
    OSSemPost(&WRITE_SEM);
    OSTimeDly(TICKS_PER_SECOND/2);
    StartCapture();
}

/*
 * Function: StopStreaming
 * -----------------------
 * Connected to UI, user stops streaming
 */
void StopStreaming() {
    streaming = 2;  // done streaming, confirmation phase
    SetCustomNetDoRX(clearedNetDoRX);
    // informations needed for confirmation page
    CapturedNum = totalP;
    finalInSD = totalInSD;
}

/*
 * Function: ResetStreaming
 * ------------------------
 * Finished capture is confirmed by user, reset app to initial setting
 */
void ResetStreaming() {
    streaming = 0;
    WriteToSocket();
}

/*
 * Function: Stream
 * ----------------
 * increment buffer in memory by captured packet and write to the SD card 
 */
inline void StreamFunc() {
    OSSemPend(&WRITE_SEM, 0);
    
    u_long fSize = 0;
    uint32_t sdBuffIndex = 0;
    packet * packetToRead;  // resides in memory
    pcapPHeader * packetToWrite;  // to SD card
    DWORD capLen;  
            
    F_FILE* fp = f_open( nameBuffer, "w+" );
    int fd = registerFFILE(fp);

    if (fp && (fd >= 0)) {
        // write file header straight to the new file
        writeall(fd, (char*) &header, sizeof(fileHeader));
        fSize += sizeof(fileHeader);

        while ((fSize < MAX_STREAM_SPACE)
                && (streaming == 1 || (totalInSD < totalP))) {

            // write when waiting on incoming packets - no downtime
            if (OSSemPendNoWait(&BUF_SEM) == OS_TIMEOUT) {
                    writeall (fd, (char*) &sdBuffer, sdBuffIndex);
                fSize += sdBuffIndex;
                sdBuffIndex = 0;
                iprintf("!");
            } else {
                OSSemPost(&BUF_SEM);
            }

            // wait for packets
            if(OSSemPend(&BUF_SEM, TICKS_PER_SECOND*2) != OS_TIMEOUT) {


                // locate next captured storage and capacity
                packetToRead = (packet*) (capBuffer + capBuffIndex);
                capLen = packetToRead -> lenPdata;

                // next packet will over flow SD buffer, write and reset
                if ((sdBuffIndex + capLen + sizeof(pcapPHeader)) 
                        > SD_BUFF_SIZE) {
                    writeall (fd, (char*) &sdBuffer, sdBuffIndex);
                    iprintf(".");
                    fSize += sdBuffIndex;
                    sdBuffIndex = 0;
                }

                // locate place to write in sdBuffer
                packetToWrite = (pcapPHeader*) (sdBuffer + sdBuffIndex);

                // populate it
                packetToWrite->ts_sec = packetToRead->time;
                packetToWrite->ts_usec = packetToRead->timeFrac;
                packetToWrite->incl_len = packetToRead->lenPdata;
                packetToWrite->orig_len = packetToRead->lenPdata;
                memcpy(packetToWrite->pData, packetToRead->pData, capLen);

                // increment both buffer
                if (capBuffIndex + capLen + sizeof(packet)
                        + BUFF_PADDING > CAP_BUFF_SIZE) {
                    capBuffIndex = 0;
                } else {
                    capBuffIndex += (capLen + sizeof(packet));
                }

                sdBuffIndex += (capLen + sizeof(pcapPHeader));

                totalInSD++; // count packets went in sd buffer
            }
        }

        // write residue packets in sdBuff
        if (sdBuffIndex) {
            writeall (fd, (char*) &sdBuffer, sdBuffIndex);
            fSize += sdBuffIndex;
            sdBuffIndex = 0;
            totalInSD = totalP;
        }

        close(fd);
        f_close(fp);
    }
    iprintf("\nSaved captured packets in file: \"%s\" with Size: %d Bytes \n"
                , nameBuffer, fSize);
    
    if ((totalP != 0) && (totalInSD != 0)) {
        iprintf ("Total packets RECORDED: %d|STORED: %d\n", totalP, totalInSD);
        CapturedNum = totalP;
        finalInSD = totalInSD;
        WriteToSocket();
        totalP = 0;
        totalInSD = 0;
    }
    OSSemPost(&DL_SEM);
    fSize = 0;
}

/*
 * Function: StreamTask
 * --------------------
 * OS Task (Priority = 48), streams capture buffer to SD card file system
 * put data into pcap format as streaming; uses secondary buffer sdBuff
 * to organize traffic and data.
 */
void StreamTask(void *p) {
    // setting the file system
    f_enterFS();
    int rv = f_chdrive(drv);

    if (rv == F_NO_ERROR) {
        while(1) {
            StreamFunc();
        }
    } else {
        iprintf( "Drive change failed in Stream Task in Stream.cpp");
        DisplayEffsErrorCode( rv );
    }
}


