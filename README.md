# 417_SniPCAP #

 This application captures Ethernet packages in the traffic in promiscuous  
 mode, stream them to SD 

 Note for developers: before flashing the program to the board, you must run: 

 ...\Nburn\pcbin\longfilename.bat 

 and Rebuild All System Files on NBEclipse

 Hardware specification changes can be made in DeviceSpec.h  

* 50MB memory buffer (module memory)
* 2 KB SRAM buffer (module RAM)  
* 4 GB file size limit for each file (FAT32)

 To view article on this product, visit www.netburner.com/learn/ or 
 http://acsweb.ucsd.edu/~bokang/blog.html

### Hardware ###
[NetBurner MOD5441X development kit](http://www.netburner.com/products/core-modules/mod5441x#kit)

### Version Log ###

 1.0 - 6/26/2015

   + added filter: if dest of the package is set to MY_MAC_ADDR
                  do nothing with it
   + added web ui: start,stop,rest,download button, and functionality)
   + changed name SnaPCAP -> SniPCAP
   + download fixed - no longer take user straight to ftp

 1.1 - 6/30/2015

   + structure fixes
   + main organized, cleaned

 1.2 - 7/6/2015

   + changed libraries for hardware switch enable;
   + EnablePromiscuous() added
   + public license added

 2.0 - 7/17/2015

   + integration of SD card feature - now streaming!
   + 4GB storage per file
 
 2.1 - 7/17/2015

   + streamed captured file is now downloadable through ftp server
   + web UI associated directly to the buffer deleted
 
 2.2 - 7/21/2015

   + file system display, download from it
   + fixed nBurn example where effs does not properly work with directories
     (improper url decoding ex. %20 -> ' ', and not recurse through DIR)
   + effs_time added
   + DeviceSpec and ftps.cpp separated from main
   + now displaying size
   + now its able to start a new file by starting the capture
   + capture file name is now editable
   
 2.3 - 7/28/2015
 
   + fixed so that landing page is index.htm
   
 2.4 - 8/4/2015
 
   + fixed bug on refreshing page with "?1" URL argument
   + placed lock on refresh page
   + implemented ajax and websocket to dynamically load page
   + myDoGet pext initialization replaced
   
 2.5 - 8/5/2015
 
   + fixed crash on initial load with "?1" URL argument
   + revisited UI design, added in-progress gif
   + fixed /DIR // by running /pcbin/longfilename.bat
   
 2.6 - 8/7/2015
 
   + major refactoring, major code clean up
   
 2.7 - 8/10/2015
 
   + got rid of AJAX call; found a way to use variable straight from the system
   + improved debug message on failure DeviceSpec comment
   + stream() renamed to streamFunc()
   + fixed logic flaw in stream task, while goes inside of if
   + added download button on finish capture


### Notes ###

417_SniPCAP copyright (c) 2015 by
ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
and NetBurner, Inc. (http://www.netburner.com/)
Distributed under the BSD 3-Clause License
See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause