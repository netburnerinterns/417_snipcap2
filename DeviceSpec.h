/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef DEVICESPEC_H
#define DEVICESPEC_H

#define DEFAULT_FILENAME "Sample\0"
#define DEFAULT_FILENAMEEX "Sample.pcap\0"
#define WEBSOCKET_URL "WS"

// 50 MB Maximum store size for MOD 54417
// CAP_BUFF_SIZE must ALWAYS be greater than BUFF_PADDING
#define CAP_BUFF_SIZE (50 * 1024 * 1024)
#define SD_BUFF_SIZE (2 * 1024) // 2 KB RAM
// 4GB limit each file
#define MAX_STREAM_SPACE (4 * 1024 * 1024 * 1024 - SD_BUFF_SIZE)
/*
 * the largest ethernet packet is 1500, BUFF_PADDING pads the head and tail
 * in case of the over run and also pads the END of the CAP_BUFF
 * so that CAP_BUFF could safely be circular
 */
#define BUFF_PADDING (4 * 1024)

#define HTTP_BUFFER_SIZE (32 * 1024)
// do not change priority unless you know what you are doing
#define WRITE_PRIO ( MAIN_PRIO - 3 )
#define FTPD_PRIO  ( MAIN_PRIO - 2 )
#define SOCKET_PRIO ( MAIN_PRIO - 1 )

#endif /* DEVICESPEC_H */

