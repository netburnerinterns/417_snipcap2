/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef STREAM_H
#define STREAM_H

#include "DeviceSpec.h"
#include "ConnToWeb.h"

// Semaphore to lock streaming
extern OS_SEM WRITE_SEM;
// Semaphore to recognize traffic
extern OS_SEM BUF_SEM;
// Semaphore for writing to be done
extern OS_SEM DL_SEM;
// status of the app: 0 - halt, 1 - streaming, 2 - finished/confirmation
extern int streaming;

// Connected to UI
void StartStreaming();
void StopStreaming();
void ResetStreaming();

#endif /* STREAM_H */

