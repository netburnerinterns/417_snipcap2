#ifndef HTMLVAR_H_
#define HTMLVAR_H_

#include <tcp.h>

extern int streaming;  // state of the application
extern uint32_t CapturedNum;  // total number of captured packet
extern uint32_t finalInSD;    // total number of packets recorded to SD Card
// used to communicate user defined names
extern char nameBuffer[260];

#endif /* HTMLVAR_H_ */
