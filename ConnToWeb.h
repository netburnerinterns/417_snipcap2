/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef CONNTOWEB_H
#define CONNTOWEB_H

#include <http.h>
#include "DeviceSpec.h"

// used to communicate user defined names
extern char nameBuffer[260];
// wait until socket connection is established
extern OS_SEM SOC_SEM;
extern int drv;

extern uint32_t CapturedNum;  // total number of captured packet 
extern uint32_t finalInSD;    // total number of packets recorded to SD Card 

// retrieve user defined name
char* getFileName ();  
/*
 * writes 4 requested data to the socket
 * 1. streaming status
 * 2. CapturedNum
 * 3. finalInSD
 * 4. user defined file name 
 */ 
void WriteToSocket();

// Upgrade the websocket so that handshake can be completed with upgrade headers
int MyDoWSUpgrade(HTTP_Request* req, int sock, PSTR url, PSTR rxb);
// websocket handler
extern http_wshandler* TheWSHandler;
// prevent refreshing web page to re initiate action taken within the page
void lockRefresh();
// re-enable refreshing web page to re initiate action taken within the page
void unlockRefresh();
// retrieve lock value
int getRefreshLock();
// OS task for web socket management  
void SocketTask (void *p );

#endif /* CONNTOWEB_H */

