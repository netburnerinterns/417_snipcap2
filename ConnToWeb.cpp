/*
 * 417_SniPCAP (https://bitbucket.org/netburnerinterns/417_SniPCAP)
 * Copyright (c) 2015 by
 *
 * ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <websockets.h>
#include <ctype.h>
#include "ConnToWeb.h"
#include "Stream.h"
#include <effs_fat/multi_drive_mmc_mcf.h>

static char HTTP_buffer[HTTP_BUFFER_SIZE] __attribute__((aligned(16)));
char inputName[253] = {DEFAULT_FILENAME};
char nameBuffer[260] = {DEFAULT_FILENAMEEX};

OS_SEM SOC_SEM;

int drv = 0;
int socket_fd = -1;
int refreshLock = 0;

fd_set write_fds;
fd_set error_fds;

extern "C" {
    void RegisterPost();
    void WebListDir(int sock, const char *dir, char* pName);
}


/*****************************************************************************
         USER DEFINE FILE NAME FUNCTIONS
******************************************************************************/
/*
 * Function: getFileName
 * ---------------------
 * Connected to web to display current set file name.
 */
char* getFileName () {
    return inputName;
}

/*
 * Function: setFileName
 * ---------------------
 * Set the name of the pcap file to store (given by user through the UI).
 * default : Sample.pcap
 * if user enters name starts with space or leaves it blank use default name
 */
void setFileName (PSTR url) {
    // parse the extension
    char *pext = url + strlen(url);

    while ((*pext != '.') && (*pext != '/') && (*pext != '\\') && (pext > url)){
        pext--;
    }
    if ((*pext == '.') || (*pext == '\\') || (*pext == '/')) {
        pext++;
    }

    // size check for the input name
    if (strlen(url) < 243) {
        strncpy(inputName, (pext + 10), 253);
    } else {
        strncpy(inputName, DEFAULT_FILENAME, 253);
    }

    // if no given input name or only space is entered, use default file name
    if (!inputName[0] || inputName[0] == '+'){
        strncpy(inputName, DEFAULT_FILENAME, 253);
    }

    siprintf(nameBuffer, "%s.pcap\0", inputName);
}


/*****************************************************************************
         WEB COMMUNICATION FUNCTIONS
******************************************************************************/
/*
 * Function: SocketTask
 * --------------------
 * OS Task (Priority = 49), waits on the server for a message.
 * Sets up the socket and listens to them, also checks closed socket
 */
void SocketTask (void *p ) {
    while (1) {
        OSSemPend(&SOC_SEM, 0);

        while(socket_fd > 0) {
            FD_SET(socket_fd, &write_fds);
            FD_SET(socket_fd, &error_fds);

            select(1, NULL, &write_fds, &error_fds, 0);

            // client closed the socket
            if (FD_ISSET(socket_fd, &error_fds)) {
                OSTimeDly(1);
                close(socket_fd);
                socket_fd = -1;
                break;
            }
        }
        FD_ZERO(&write_fds);
        FD_ZERO(&error_fds);

        OSTimeDly(TICKS_PER_SECOND);
    }
}

/*
 * Function: WriteToSocket
 * -----------------------
 * Sends out required data through web sockets
 */
void WriteToSocket() {
    if(socket_fd > 0) {

        FD_SET(socket_fd, &write_fds);
        FD_SET(socket_fd, &error_fds);

        select(1, NULL, &write_fds, &error_fds, 0);

        // write to socket
        if (FD_ISSET(socket_fd, &write_fds)) {
            char out[400];
            siprintf(out, "{\"streaming\":%i, \"totalP\":%i, \"totalInSD\":%i, \"finalName\":\"%s\" }", streaming, CapturedNum, finalInSD, nameBuffer);
            writeall(socket_fd, out, strlen(out));
            NB::WebSocket::ws_flush(socket_fd);
    }

        FD_ZERO(&write_fds);
        FD_ZERO(&error_fds);
    }
}

/*
 * Function: NotAvailableResponse
 * ------------------------------
 * Error catching function on limited resource and printout appropriate error message
 */
void NotAvailableResponse( int sock, PCSTR url ) {
   writestring( sock,
         "HTTP/1.0 503\r\nPragma: no-cache\r\nContent-Type: text/html\r\n\r\n" );
   writestring( sock,
         "<HEAD><TITLE>503 Not Available</TITLE></HEAD>\n<BODY><H1>503 Not Available</H1>\nServer has insufficient resources to service the request for the URL: " );
   writestring( sock, url );
   writestring( sock, "<BR>\n</BODY>\r\n" );
}

/*
 * Function: MyDoWSUpgrade
 * -----------------------
 * Upgrade the websocket so that handshake can be completed with upgrade headers
 */
int MyDoWSUpgrade(HTTP_Request* req, int sock, PSTR url, PSTR rxb) {
    if (httpstricmp(url, WEBSOCKET_URL)) {
        if (socket_fd < 0) {
            int rv = WSUpgrade(req, sock);

            if (rv >= 0) {
                socket_fd = rv;
                NB::WebSocket::ws_setoption(socket_fd, WS_SO_TEXT);
                OSSemPost(&SOC_SEM);
                return 2;

            } else {
                return 0;
            }
        }
        else {
            NotAvailableResponse(sock, url);
            return 0;
        }
    }
    NotFoundResponse(sock, url);
    return 0;
}


/*****************************************************************************
         DISPLAY WEB DIR FUNCTIONS
******************************************************************************/
/*
 * Function: SendFragment
 * ----------------------
 *  helper function to WebListDir that sends contents of HTTP_BUFFER 
 */
void SendFragment(int sock, F_FILE *f, long len) {
    int lread = 0;
    while (lread < len) {
        int ltoread = len - lread;
        int lr;

        if (ltoread > HTTP_BUFFER_SIZE) ltoread = HTTP_BUFFER_SIZE;

        lr = f_read(HTTP_buffer, 1, HTTP_BUFFER_SIZE, f);

        if (lr == 0) return;

        lread += lr;
        writeall(sock, HTTP_buffer, lr);
    }
}

/*
 * Function: WebListDir
 * ----------------------
 * Displays a list of directories and files to the web browser;
 * enables user to download contents of SD card.
 */
void WebListDir(int sock, const char *dir, char* pName) {


    char buffer[150];
    writestring(sock, "HTTP/1.0 200 OK\r\n");
    writestring(sock, "Pragma: no-cache\r\n");
    writestring(sock, "MIME-version: 1.0\r\n");
    writestring(sock, "Content-Type: text/html\r\n\r\n");
    writestring(sock, "<html>\r\n");
    writestring(sock, "   <body>\r\n");
    writestring(sock, "      <h2><font face=\"Arial\">Directory of ");
    writestring(sock, dir);
    writestring(sock, "</font></h2>\r\n");
    writestring(sock, "      <hr>\r\n");
    writestring(sock, "      <ul><font face=\"Courier New\" size=\"2\">\r\n");

    F_FIND f;

    // change dir when the function is recursively called
    if (httpstricmp(pName, "DIR")) {
          f_chdir(dir);
    }

    int rc = f_findfirst("*.*", &f);

    while (rc == 0) {
        if (f.attr & F_ATTR_DIR) {
            writestring(sock, "<li><img src=\"/images/folder.gif\"><a href=\"");
            writestring(sock, f.filename);
            writestring(sock, "/DIR\">");
            writestring(sock, f.filename);
            writestring(sock, "</a>\r\n");
        }
        else {
            if (strstr(f.filename, ".pcap")){
                writestring(sock, "<li><img src=\"/images/pcap_icon.png\"><a href=\"");
                writestring(sock, f.filename);
                writestring(sock, "\">");
                writestring(sock, f.filename);
                writestring(sock, "</a>\r\n");
            } else {
                writestring(sock, "<li><img src=\"/images/text.gif\"><a href=\"");
                writestring(sock, f.filename);
                writestring(sock, "\">");
                writestring(sock, f.filename);
                writestring(sock, "</a>\r\n");
            }
        }
        rc = f_findnext(&f);
    }
    writestring(sock, "      </font></ul>\r\n");
    writestring(sock, "      <hr>\r\n");
    siprintf(buffer, "<a href=\"/INDEX.HTM\" target=\"_parent\"><button>Back to UI</button></a>");
    writestring(sock, buffer);
    writestring(sock, "   </body>\r\n");
    writestring(sock, "</html>");
}


/*****************************************************************************
         WEB GET REQUEST FUNCTIONS
******************************************************************************/
/*
 * Function: urldecode2
 * --------------------
 * takes URL as argument and decode it i.e. %20 -> ' '
 * based from Multimedia Mike in stackoverflow.com and fixed to work properly.
 * source: http://stackoverflow.com/questions/2673207/c-c-url-decode-library
 */
void urldecode2(char *dst, const char *src) {
    char a, b;
    while (*src) {
        if ((*src == '%') && ((a = src[1]) && (b = src[2]))
                && (isxdigit(a) && isxdigit(b))) {
            if (a >= 'a') {
                a -= 'A'-'a';
            }

            if (a >= 'A') {
                a -= ('A' - 10);
            } else {
                a -= '0';
            }

            if (b >= 'a') {
                b -= 'A'-'a';
            }

            if (b >= 'A') {
                b -= ('A' - 10);
            } else {
                b -= '0';
            }

            *dst++ = 16*a+b;
            src+=3;
        } else {
            *dst++ = *src++;
        }
    }
    *dst++ = '\0';
}

static http_gethandler *oldhand;
/*
 * Function: MyDoGet
 * -----------------
 * This function intercepts the web browser's GET request
 */
int MyDoGet(int sock, PSTR url, PSTR rxBuffer) {
    char name_buffer[257] = {}; // Reserve last byte for null character
    char temp_buffer[257] = {};
    char dir_buffer[256] = {};
    char ext_buffer[10] = {};
    PSTR oldUrlPtr = url;

    f_chdrive(drv);
    f_chdir("\\");

    urldecode2(temp_buffer, url);
    url = temp_buffer;

    // Parse and store file extension portion of URL
    char *pext = url + strlen(url);
    int key = 0;
    char tempCheck[strlen(url)];

    for (uint32_t i = 0; i < strlen(url); i++) {
        if (url[i] == '?' && i + 4 <= strlen(url)) {
            i++;

            strncpy (tempCheck, &url[i], strlen(tempCheck));

            if (httpstricmp(tempCheck, "FNAME")) {
                key = 1;
            }

            break;
        }
    }

    // if file name is given set it
    if (key) {
        key = 0;
        setFileName(url);
    }

    while ((*pext != '.') && (*pext != '/') && (*pext != '\\') && (pext > url)){
        pext--;
    }
    if ((*pext == '.') || (*pext == '\\') || (*pext == '/')) {
        pext++;
    }

    strncpy(ext_buffer, pext, 9);

    // Parse and store file name portion of URL
    char *pName = url + strlen(url);

    while ((*pName != '/') && (*pName != '\\') && (pName > url)) {
        pName--;
    }
    if ((*pName == '\\') || (*pName == '/')) {
        pName++;
    }

    strncpy(name_buffer, pName, 256);

    // Store directory portion of URL
    strncpy(dir_buffer + 1, url, (pName - url));
    dir_buffer[0] = '/';
    dir_buffer[(pName - url) + 1] = 0;

    // If extension is a DIR display sub directory listing
    if ( f_chdir(dir_buffer) == F_NO_ERROR) {
        if (name_buffer[0] == 0) {
            if (dir_buffer[1] == 0) {
                // Root file try index.ht* first
                F_FIND f;
                int rc = f_findfirst("index.ht*", &f);

                if (rc != 0) {
                    rc = f_findfirst("*.htm", &f);
                    if (rc != 0) rc = f_findfirst("*.html", &f);
                }
                if (rc == 0) {
                    RedirectResponse(sock, f.filename);
                    return 0;
                }
            }
        }
        else {
            // A file name was specified in the URL, so attempt to open it
            F_FILE *f = f_open(pName, "r");

            if (f != NULL) {
                long len = f_filelength(pName);
                SendFragment(sock, f, len);
                f_close(f);
                return 0;
            }

            /* If the work "DIR" is specified in the URL at any directory level,
               then this code will result in a directory listing. */
            if (httpstricmp(pName, "DIR")) {
                WebListDir(sock, dir_buffer, pName);
                return 0;
            }
        }
    }
    return (*oldhand)(sock, oldUrlPtr, rxBuffer);
}

/* register our new hander */
void RegisterPost() {
    oldhand = SetNewGetHandler(MyDoGet);
}



/*****************************************************************************
         REFRESH PAGE LOCK FUNCTIONS
******************************************************************************/
/*
 * Function: lockRefresh
 * ---------------------
 * prevent refreshing web page to re initiate action taken within the page
 */
void lockRefresh() {
    refreshLock = 1;
}

/*
 * Function: unlockRefresh
 * -----------------------
 * re-enable refreshing web page to re initiate action taken within the page
 */
void unlockRefresh() {
    refreshLock = 0;
}

/*
 * Function: getRefreshLock
 * ------------------------
 * retrieve lock value
 */
int getRefreshLock() {
    return refreshLock;
}



